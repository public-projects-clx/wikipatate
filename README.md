## Wikipatate
Principe : Jeu, une page wikipedia est choisie aléatoirement, le but du jeu est de rejoindre en le moins de liens possible la page “Pomme de terre” (uniquement grâce aux liens internes Wikipedia).
### Back - ExpressJS + Typescript
- Gestion du status des pages :
    - Vérifier si le statut de la page est égale à 200
	- Enlever les liens externes de la page
- Gestion du nom de la page :
	- Récupération du nom d’une page aléatoire
	- Vérfier si le nom n’est pas égale à pomme de terre
	- Vérfier si la page renvoyée est pomme de terre pour mettre fin à la partie
- Gestion des stats :
	- Record pour l’article au hasard (liste des liens suivis)
### Front - VueJS
- Layout du jeu
- Encart pour afficher la page wikipedia
- Encart pour afficher la partie en cours (nombre de liens cliqués, liste des liens)
- Popup de fin :
	- affichage d’un message si c’est le record
	- affichage du record (nombre de liens, et liste des liens)
	- relancer une partie
	- quitter
### Evolutions possibles
- Gestion difficulté :
	- max de liens possibles
	- rubriques désactivés (pays par exemple)
	- temps