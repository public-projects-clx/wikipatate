import { Request, Response, Router } from 'express';
import { BAD_REQUEST, OK } from 'http-status-codes';

const router = Router();

router.get('/ping', async (req: Request, res: Response) => {
    return res.status(OK).json({
        ping: true
    });
});

export default router;
